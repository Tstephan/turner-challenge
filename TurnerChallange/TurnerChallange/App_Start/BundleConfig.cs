﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace TurnerChallange
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {

            //Add some bundles
            bundles.Add(new ScriptBundle("~/bundles/angular")
                .Include(
                    "~/Scripts/angular.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap")
                .Include(
                    "~/Scripts/bootstrap.js",
                    "~/Scripts/jquery-1.9.1.js"
                ));

            bundles.Add(new StyleBundle("~/bundles/bootstrap-css")
                .Include(
                    "~/Content/bootstrap-theme.css",
                    "~/Content/bootstrap.css"
                ));
        }
    }
}
