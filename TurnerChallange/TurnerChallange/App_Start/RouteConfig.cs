﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TurnerChallange
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {

            //Do the routes
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "HomeRedirect",
                url: "Home/Index",
                defaults: new { controller = "Home", action = "Index", redirect = true }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}",
                defaults: new { controller = "Home", action = "Index" }
            );

            routes.MapRoute(
                name: "Error404_URL",
                url: "{*url}",
                defaults: new { controller = "Home", action = "Index", redirect = true }
            );


        }
    }
}
