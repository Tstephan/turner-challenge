﻿var mod = angular.module('application', []);
mod.controller('titlesController', function ($scope, TitlesService) {

	TitlesService.getTitles().then(resolve, reject);

	$scope.currentTitle = undefined;
	$scope.openModal = function (title) {
		$scope.currentTitle = title;
		$('#titleModal').modal('toggle');
	};

	function resolve(response) {
		var data = response.data;
		$scope.titles = data;
	}

	function reject(error) {
		console.log(error);
	}

});

mod.factory('TitlesService', ['$http', function ($http) {

	var service = {
		getTitles: function () {
			return $http.get('Home/GetTitles');
		}
	};

	return service;
}]);