﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TurnerChallange.Models
{
    public class GenreDTO
    {
        public int titleId { get; set; }
        public String genre { get; set; }
    }
}