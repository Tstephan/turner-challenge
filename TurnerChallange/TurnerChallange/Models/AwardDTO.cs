﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TurnerChallange.Models
{
    public class AwardDTO
    {
        public int titleId { get; set; }
        public String name { get; set; }
        public int? year { get; set; }
        public Boolean? won { get; set; }
        public String awardCompany { get; set; }
    }
}