﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TurnerChallange.Models
{
    public class TitleDTO
    {
        public int id { get; set; }
        public String name { get; set; }
        public int? releaseYear { get; set; }
        public List<GenreDTO> genres { get; set; }
        public List<ParticipantDTO> participants { get; set; }
        public List<AwardDTO> awards { get; set; }
        public List<AltTitleDTO> altTitles { get; set; }
        public List<StorylineDTO> storylines { get; set; }
    }
}