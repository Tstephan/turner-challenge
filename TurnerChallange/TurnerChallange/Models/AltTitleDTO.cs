﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TurnerChallange.Models
{
    public class AltTitleDTO
    {
        public int titleId { get; set; }
        public String name { get; set; }
        public String language { get; set; }
        public String type { get; set; }
    }
}