﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TurnerChallange.Models
{
    public class ParticipantDTO
    {
        public int titleId { get; set; }
        public String name { get; set; }
        public String roleType { get; set; }
        public Boolean isKey { get; set; }
        public Boolean isOnScreen { get; set; }
    }
}