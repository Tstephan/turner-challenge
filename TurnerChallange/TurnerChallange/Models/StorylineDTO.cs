﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TurnerChallange.Models
{
    public class StorylineDTO
    {
        public int titleId { get; set; }
        public String source { get; set; }
        public String description { get; set; }
    }
}