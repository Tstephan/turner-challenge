﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using TurnerChallange.Models;

namespace TurnerChallange.Controllers.Home
{
    public class HomeController : Controller
    {
        //Have some actions ready
        public ActionResult Index(bool? redirect)
        {
            if (redirect != null && redirect == true)
                return RedirectToActionPermanent("Index", new { redirect = null as object });

            return View();
        }

        public ActionResult NotFound()
        {
            return RedirectToActionPermanent("Index", new { redirect = null as object });
        }

        public ActionResult InternalError()
        {
            return RedirectToActionPermanent("Index", new { redirect = null as object });
        }

        //The GET for those titles from the db
        [HttpGet]
        public JsonResult GetTitles()
        {
            
            //Database context
            TitlesDatabase db = new TitlesDatabase();

            //Linq for everything!
            var titles = (from t0 in db.Titles
                          select new
                          {
                              titleId = t0.TitleId,
                              titleName = t0.TitleName,
                              releaseYear = t0.ReleaseYear
                          });

            var participants = (from t1 in db.Titles
                                join tp in db.TitleParticipants on t1.TitleId equals tp.TitleId
                                join p in db.Participants on tp.ParticipantId equals p.Id
                                select new
                                {
                                    titleId = t1.TitleId,
                                    participantName = p.Name,
                                    roleType = tp.RoleType,
                                    isKey = tp.IsKey,
                                    isOnScreen = tp.IsOnScreen
                                });

            var genres = (from t2 in db.Titles
                           join tg in db.TitleGenres on t2.TitleId equals tg.TitleId
                           join g in db.Genres on tg.GenreId equals g.Id
                           select new
                           {
                               titleId = t2.TitleId,
                               genere = g.Name
                           });

            var awards = (from t3 in db.Titles
                          join a in db.Awards on t3.TitleId equals a.TitleId
                          select new
                          {
                              titleId = t3.TitleId,
                              awardName = a.Award1,
                              awardYear = a.AwardYear,
                              awardWon = a.AwardWon,
                              awardCompany = a.AwardCompany,
                          });

            var altTitles = (from t4 in db.Titles
                             join o in db.OtherNames on t4.TitleId equals o.TitleId
                             where o.TitleNameType != "Primary"
                             select new
                             {
                                 titleId = t4.TitleId,
                                 altTitleName = o.TitleName,
                                 altTitleNameLanguage = o.TitleNameLanguage,
                                 altTitleNameType = o.TitleNameType
                             });

            var storylines = (from t5 in db.Titles
                              join s in db.StoryLines on t5.TitleId equals s.TitleId
                              select new
                              {
                                  titleId = t5.TitleId,
                                  source = s.Type,
                                  description = s.Description
                              });


            //Queries as lists
            var titlesList = titles.ToList();
            var participantsList = participants.ToList();
            var genresList = genres.ToList();
            var awardsList = awards.ToList();
            var altTitlesList = altTitles.ToList();
            var storylinesList = storylines.ToList();


            //Put the data together for everything
            List<TitleDTO> data = new List<TitleDTO>();

            TitleDTO titleObj;
            for (int i = 0; i < titlesList.Count; ++i)
            {
                var cur = titlesList[i];

                //Create a base title
                titleObj = new TitleDTO();
                titleObj.id = cur.titleId;
                titleObj.name = cur.titleName;
                titleObj.releaseYear = cur.releaseYear;
                titleObj.genres = new List<GenreDTO>();
                titleObj.participants = new List<ParticipantDTO>();
                titleObj.awards = new List<AwardDTO>();
                titleObj.altTitles = new List<AltTitleDTO>();
                titleObj.storylines = new List<StorylineDTO>();

                //Add the particpants
                for (int j = 0; j < participantsList.Count; ++j)
                {
                    var curPart = participantsList[j];
                    if (titleObj.id == curPart.titleId)
                    {
                        titleObj.participants.Add(new ParticipantDTO()
                        {
                            titleId = curPart.titleId,
                            name = curPart.participantName,
                            roleType = curPart.roleType,
                            isKey = curPart.isKey,
                            isOnScreen = curPart.isOnScreen,
                        });
                    }
                }

                //Add the genres
                for (int j = 0; j < genresList.Count; ++j)
                {
                    var curGenre = genresList[j];
                    if (titleObj.id == curGenre.titleId)
                    {
                        titleObj.genres.Add(new GenreDTO()
                        {
                            titleId = curGenre.titleId,
                            genre = curGenre.genere
                        });
                    }
                }

                //Add the awards
                for (int j = 0; j < awardsList.Count; ++j)
                {
                    var curAward = awardsList[j];
                    if (titleObj.id == curAward.titleId)
                    {
                        titleObj.awards.Add(new AwardDTO()
                        {
                            titleId = curAward.titleId,
                            name = curAward.awardName,
                            year = curAward.awardYear,
                            won = curAward.awardWon,
                            awardCompany = curAward.awardCompany
                        });
                    }

                }

                //Add the alt titles
                for (int j = 0; j < altTitlesList.Count; ++j)
                {
                    var curAltTitle = altTitlesList[j];
                    if (titleObj.id == curAltTitle.titleId)
                    {
                        titleObj.altTitles.Add(new AltTitleDTO()
                        {
                            titleId = curAltTitle.titleId,
                            name = curAltTitle.altTitleName,
                            type = curAltTitle.altTitleNameType,
                            language = curAltTitle.altTitleNameLanguage
                        });
                    }

                }

                //Add the story lines
                for (int j = 0; j < storylinesList.Count; ++j)
                {
                    var curStoryline = storylinesList[j];
                    if (titleObj.id == curStoryline.titleId)
                    {
                        titleObj.storylines.Add(new StorylineDTO()
                        {
                            titleId = curStoryline.titleId,
                            source = curStoryline.source,
                            description = curStoryline.description,
                        });
                    }

                }

                //Add the finalized title to the list
                data.Add(titleObj);
            }

            //Return everything!
            return Json(data, JsonRequestBehavior.AllowGet);
        }

    }
}