# README #

### Information ###
* Create for the Turner Developer Challenge
* 
* This project was created in Visual Studio 2017 using .Net Framework 4.5.2 and EntityFramework 6.1.3 
* It requires the following packages from NuGet: AngularJs 1.6.3, BootStrap by Twitter 3.3.7
* The database used for the project was SQL Server Express 2012 as specified by the Turner Challenge. Option 3 (SQL Script) was used to create the database
